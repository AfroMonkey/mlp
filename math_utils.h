#ifndef MATH_UTILS_HPP
#define MATH_UTILS_HPP

#include <string>
#include <vector>

namespace mu {
    namespace matrix {
        bool is_matrix(const std::vector<std::vector<double>> &m);

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<std::vector<double>> &m1,
          const std::vector<std::vector<double>> &m2);

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<double> &v1,
          const std::vector<std::vector<double>> &m2);

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<std::vector<double>> &m1,
          const std::vector<double> &v2);

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<double> &v1, const std::vector<double> &v2);

        std::vector<std::vector<double>> transpose(
          const std::vector<std::vector<double>> &m);

        std::vector<std::vector<double>> transpose(
          const std::vector<double> &v);

        std::vector<std::vector<double>> diagonal(const std::vector<double> &v);

        std::string to_string(const std::vector<std::vector<double>> &m,
                              bool arch = false);

        std::string to_string(const std::vector<double> &v, bool arch = false);
    }

    namespace activation_function {
        double identity(double y);

        double identity_d(double y);

        double sigmoid(double y);

        double sigmoid_d(double y);
    }
}

#endif // MATH_UTILS_HPP
