#include "math_utils.h"

#include <cmath>
#include <stdexcept>
#include <string>
#include <vector>

namespace mu {
    namespace matrix {
        bool is_matrix(const std::vector<std::vector<double>> &m) {
            for (auto row : m) {
                if (row.size() != m[0].size()) {
                    return false;
                }
            }
            return true;
        }

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<std::vector<double>> &m1,
          const std::vector<std::vector<double>> &m2) {
            std::vector<std::vector<double>> m3;
            auto m1_rows = m1.size();
            auto m1_cols = m1[0].size();
            auto m2_rows = m2.size();
            auto m2_cols = m2[0].size();

            if (not is_matrix(m1)) {
                throw std::length_error("arg1 is not a matrix");
            }
            if (not is_matrix(m2)) {
                throw std::length_error("arg2 is not a matrix");
            }
            if (m1_cols != m2_rows) {
                throw std::length_error(
                  "Incongruent dimensions " + std::to_string(m1_rows) + "x" +
                  std::to_string(m1_cols) + " @ " + std::to_string(m2_rows) +
                  "x" + std::to_string(m2_cols));
            }

            m3.resize(m1_rows);
            for (auto &row : m3) {
                row.resize(m2_cols, 0);
            }

            for (size_t i = 0; i < m1_rows; ++i) {
                for (size_t j = 0; j < m2_cols; ++j) {
                    for (size_t k = 0; k < m1_cols; ++k) {
                        m3[i][j] += m1[i][k] * m2[k][j];
                    }
                }
            }
            return m3;
        }

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<double> &v1,
          const std::vector<std::vector<double>> &m2) {
            return pointwise_product(std::vector<std::vector<double>>{v1}, m2);
        }

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<std::vector<double>> &m1,
          const std::vector<double> &v2) {
            return pointwise_product(m1, std::vector<std::vector<double>>{v2});
        }

        std::vector<std::vector<double>> pointwise_product(
          const std::vector<double> &v1, const std::vector<double> &v2) {
            return pointwise_product(std::vector<std::vector<double>>{v1},
                                     std::vector<std::vector<double>>{v2});
        }

        std::vector<std::vector<double>> transpose(
          const std::vector<std::vector<double>> &m) {
            std::vector<std::vector<double>> mt;
            mt.resize(m[0].size());
            for (auto &row : mt) {
                row.resize(m.size());
            }
            for (size_t i = 0; i < m.size(); ++i) {
                for (size_t j = 0; j < m[0].size(); ++j) {
                    mt[j][i] = m[i][j];
                }
            }
            return mt;
        }

        std::vector<std::vector<double>> transpose(
          const std::vector<double> &v) {
            return transpose(std::vector<std::vector<double>>{v});
        }

        std::vector<std::vector<double>> diagonal(
          const std::vector<double> &v) {
            std::vector<std::vector<double>> d;
            d.resize(v.size());
            for (size_t i = 0; i < v.size(); ++i) {
                d[i].resize(v.size());
                d[i][i] = v[i];
            }
            return d;
        }

        std::string to_string(const std::vector<std::vector<double>> &m,
                              bool arch) {
            std::string s;
            if (arch) {
                s += std::to_string(m.size()) + "x" +
                     std::to_string(m[0].size()) + "\n";
            }
            for (auto row : m) {
                for (auto col : row) {
                    s += std::to_string(col) + ' ';
                }
                s += "\n";
            }
            return s;
        }

        std::string to_string(const std::vector<double> &v, bool arch) {
            return to_string(std::vector<std::vector<double>>{v}, arch);
        }
    }

    namespace activation_function {
        double identity(double y) { return y; }

        double identity_d(double y) {
            return y / y; // FIX used to remove warning of unused parameter
        }

        double sigmoid(double y) { return 1 / (1 + exp(-y)); }

        double sigmoid_d(double y) {
            auto s = sigmoid(y);
            return s * (1 - s);
        }
    }
}
