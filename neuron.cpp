#include "neuron.h"

using namespace mu::matrix;

Neuron::Neuron(unsigned long inputs) {
    ++inputs;
    weights.resize(inputs);
    reinit();
    f = mu::activation_function::identity;
    f_d = mu::activation_function::identity_d;
}

Neuron::Neuron(unsigned long inputs, std::function<double(double val)> f,
               std::function<double(double val)> f_d) :
  f(f),
  f_d(f_d) {
    ++inputs;
    weights.resize(inputs);
    reinit();
}

void Neuron::reinit() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> urd(-1.0, 1.0);
    for (unsigned long d = 0; d < weights.size(); ++d) {
        weights[d] = urd(gen);
    }
}

double Neuron::eval(std::vector<double> point) {
    point.push_back(-1);
    last_n = pointwise_product(weights, transpose(point))[0][0];
    return last_n;
}

double Neuron::activation(const std::vector<double> &point) {
    return f(eval(point));
}

double Neuron::activation(double val) {
    return f(val);
}

double Neuron::eval_f() {
    return f(last_n);
}

double Neuron::eval_f_d() {
    return f_d(last_n);
}
