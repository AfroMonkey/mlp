#ifndef MLP_HPP
#define MLP_HPP

#include "neuron.h"

#include <vector>

class MLP {
public:
    // attribs
    std::vector<std::vector<Neuron>> layers;
    // constructor
    MLP(
      const std::vector<unsigned> &arch = {1, 1},
      const std::vector<std::function<double(double val)>> &activations = {},
      const std::vector<std::function<double(double val)>> &activations_d = {});
    // methods
    double fit(const std::vector<double> &inputs,
               const std::vector<double> &desireds, double learning_rate = 0.5);
    std::pair<double, size_t> train(
      const std::vector<std::vector<double>> &Inputs,
      const std::vector<std::vector<double>> &Desireds,
      double learning_rate = 0.5, double max_error = 0.01,
      size_t max_epochs = 1000000);
    std::string to_string();
    std::vector<std::vector<double>> eval(std::vector<double> point);
};

#endif // MLP_HPP
