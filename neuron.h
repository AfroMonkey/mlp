#ifndef NEURON_HPP
#define NEURON_HPP

#include "math_utils.h"

#include <functional>
#include <random>
#include <vector>

class Neuron {
public:
    // attribs
    std::vector<double> weights;
    std::function<double(double val)> f;
    std::function<double(double val)> f_d;
    double last_n;
    // constructor
    Neuron(unsigned long inputs = 1);
    Neuron(unsigned long inputs, std::function<double(double val)> f,
           std::function<double(double val)> f_d);
    // methods
    void reinit();
    double eval(std::vector<double> point);
    double activation(const std::vector<double> &point);
    double activation(double val);
    double eval_f();
    double eval_f_d();
};

#endif // NEURON_HPP
