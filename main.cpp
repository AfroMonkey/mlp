#include "mlp.h"

#include <iostream>

using namespace std;

int main() {
    std::vector<std::function<double(double val)>> activations = {
      mu::activation_function::sigmoid,
      mu::activation_function::sigmoid,
      mu::activation_function::sigmoid};
    std::vector<std::function<double(double val)>> activations_d = {
      mu::activation_function::sigmoid_d,
      mu::activation_function::sigmoid_d,
      mu::activation_function::sigmoid_d};
    MLP mlp({2, 2, 2, 2}, activations, activations_d);
    // double g = 0;
    // for (size_t m = 0; m < mlp.layers.size(); ++m) {
    //     for (size_t i = 0; i < mlp.layers[m].size(); ++i) {
    //         mlp.layers[m][i].weights.back() = ++g;
    //         for (size_t j = 0; j < mlp.layers[m][i].weights.size() - 1; ++j)
    //         {
    //             mlp.layers[m][i].weights[j] = ++g;
    //         }
    //     }
    // }
    // {{0, 0}, {0, 1}, {1, 0}, {1, 1}}, {{1, 0}, {0, 1}, {0, 1}, {1, 0}}, 1.0
    cout << mlp.to_string() << endl;
    mlp.train({{0, 0}, {0, 1}, {1, 0}, {1, 1}},
              {{1, 0}, {0, 1}, {0, 1}, {1, 0}},
              0.5,
              0.01);
    cout << mlp.to_string() << endl;
    cout << mu::matrix::to_string(mlp.eval({0.9, 0.7})) << endl;
    return 0;
}
