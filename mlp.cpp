#include "mlp.h"

#include <stdexcept>
#include <string>

using namespace mu::matrix;

MLP::MLP(const std::vector<unsigned> &arch,
         const std::vector<std::function<double(double val)>> &activations,
         const std::vector<std::function<double(double val)>> &activations_d) {
    bool custom = activations.size() != 0 or activations_d.size() != 0;
    if (custom) {
        if (arch.size() - 1 != activations.size()) {
            throw std::length_error(
              "must specify activation function for each layer");
        }
        if (activations.size() != activations_d.size()) {
            throw std::length_error(
              "activations and activations_d must be same size");
        }
    }
    for (size_t i = 1; i < arch.size(); ++i) {
        layers.push_back(std::vector<Neuron>());
        for (unsigned _ = arch[i]; _; --_) {
            if (custom) {
                layers.back().push_back(Neuron(
                  arch[i - 1], activations[i - 1], activations_d[i - 1]));
            } else {
                layers.back().push_back(Neuron(arch[i - 1]));
            }
        }
    }
}

double MLP::fit(const std::vector<double> &inputs,
                const std::vector<double> &desireds, double learning_rate) {
    if (inputs.size() != layers[0].size()) {
        throw std::length_error("inputs size doesn't match arch");
    }
    if (desireds.size() != layers.back().size()) {
        throw std::length_error("desireds size doesn't match arch");
    }
    // foward
    auto a_matrix = eval(inputs);
    // backward
    std::vector<std::vector<double>> sensitivities;
    sensitivities.resize(layers.size());
    // s^M
    double error = 0;
    for (size_t j = 0; j < layers.back().size(); ++j) {
        auto local_error = desireds[j] - a_matrix.back()[j];
        sensitivities.back().push_back(-2 * layers.back()[j].eval_f_d() *
                                       local_error);
        error += local_error * local_error;
    }
    error /= 2;
    // s^m
    for (int m = layers.size() - 2; m >= 0; --m) {
        std::vector<std::vector<double>> weights_mp1;
        std::vector<std::vector<double>> fs_d;
        fs_d.resize(1);
        for (auto neuron : layers[m + 1]) {
            weights_mp1.push_back(neuron.weights);
            weights_mp1.back().resize(weights_mp1.back().size() - 1);
        }
        for (auto neuron : layers[m]) {
            fs_d.back().push_back(neuron.eval_f_d());
        }
        fs_d = diagonal(fs_d[0]);
        sensitivities[m] = transpose(
          pointwise_product(pointwise_product(fs_d, transpose(weights_mp1)),
                            transpose(sensitivities[m + 1])))[0];
    }
    // update w's
    for (size_t m = 0; m < layers.size(); ++m) {
        for (size_t j = 0; j < layers[m].size(); ++j) {
            auto d = -learning_rate * sensitivities[m][j];
            a_matrix[m].push_back(-1);
            for (size_t i = 0; i < layers[m][j].weights.size(); ++i) {
                layers[m][j].weights[i] += d * a_matrix[m][i];
            }
        }
    }
    return error;
}

std::pair<double, size_t> MLP::train(
  const std::vector<std::vector<double>> &Inputs,
  const std::vector<std::vector<double>> &Desireds, double learning_rate,
  double max_error, size_t max_epochs) {
    double error;
    size_t no_used_epochs = max_epochs;
    while (no_used_epochs) {
        error = 0;
        for (size_t example = 0; example < Inputs.size(); ++example) {
            error += fit(Inputs[example], Desireds[example], learning_rate);
        }
        if (error <= max_error) {
            break;
        }
        --no_used_epochs;
    }
    return std::make_pair(error, max_epochs - no_used_epochs);
}

std::string MLP::to_string() {
    std::string arch;
    for (size_t l = 0; l < layers.size(); ++l) {
        arch += "l" + std::to_string(l + 1) + "\n";
        for (size_t n = 0; n < layers[l].size(); ++n) {
            arch += "n" + std::to_string(n) + ": ";
            for (size_t w = 0; w < layers[l][n].weights.size() - 1; ++w) {
                arch += "w" + std::to_string(w + 1) + "=" +
                        std::to_string(layers[l][n].weights[w]) + " ";
            }
            arch += "b=" + std::to_string(layers[l][n].weights.back()) + "\n";
        }
    }
    return arch;
}

std::vector<std::vector<double>> MLP::eval(std::vector<double> point) {
    std::vector<std::vector<double>> a_matrix;
    if (point.size() != layers[0].size()) {
        throw std::length_error("point size doesn't match arch");
    }
    for (long unsigned int l = 0; l < layers.size(); ++l) {
        std::vector<double> layer;
        for (long unsigned int n = 0; n < layers[l].size(); ++n) {
            layer.push_back(layers[l][n].eval(point));
        }
        a_matrix.push_back(point);
        point = layer;
    }
    a_matrix.push_back(point);
    return a_matrix;
}
