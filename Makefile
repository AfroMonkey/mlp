CC = g++
CFLAGS = -Wall -Wextra -Wfatal-errors -pedantic -std=c++17

SOURCES = *.cpp

all:
	$(CC) $(CFLAGS) $(SOURCES)
clean:
	rm exec
